<?php

/*
 * Plugin Name: Blue Heron Bridge Parking Estimator
 * Plugin URI:  https://gitlab.com/calevans/BHBParkingEstimator
 * Description: Estimate arrival times for scuba divers at the Blue Heron Bridge
 * Version:     1.0.0
 * Author:      Cal Evans
 * Author URI:  https://calevans.com
 * Text Domain: bhb-parking-estimator
 * License:     MIT
 */
if (! defined('WPINC')) {
  die();
}

require_once 'src/Plugin.php';
require_once 'src/Models/BHBParkingEstimator.php';
require_once 'src/Models/HolidayCalculator.php';
require_once 'src/Models/USHolidays.php';

(
  new BHBParkingEstimatorPlugin(
      new BHBParkingEstimator(
          new HolidayCalculator(new USHolidays())
      )
  )
)->init();
