# Blue Heron Bridge Parking Estimator

(c) 2023 E.I.C.C., Inc.<br>
All Rights Reserved

Released under the MIT License.

If you want to use this WordPress plugin, feel free to download and install. If you have questions, you can contact me at cal@calevans.com and ask.

# Installation

**WARNINGS**

- This plugin requires PHP 8.1 or higher to operate. If you are not sure what your PHP version is, don't install until you find out.
- This plugin **requires** the [Tide-Tables-Client](https://gitlab.com/calevans/tide-tables-client) plugin to be installed, activated, and properly configured. If you don't have that, this won't work.

1. Download as a zip file
1. In the WordPress admin menu, select Plugins->Add New.
1. Click the upload Plugin button.
1. Drag the Zip file into the screen.
1. Once it's installed, click on Activate.

# Use

This plugin defines a shortcode to be used in WordPress.

```
[bhb-parking-estimator]
```
This will output something that looks like this:

```
For the AM tide, you should arrive at the Blue Heron Bridge around 06:01 AM
For the PM tide, you should arrive at the Blue Heron Bridge around 06:36 PM
```

That shortcode will show you the parking estimate for today.

[bhb-parking-estimator date=? days=1 explain=false]

- **date** = The date to show parking for. If days is specific they are added to this date. If this is not specified, today is used.
- **days** = The number of days from date to check. If date is not specified then it uses todays date. Setting this to 1 will mean that you show tomorrow's parking recommendations.
- **explain** = Largely for debugging purposes. If set to 1 then it will show the modifiers used to compute the recommended times.

All parameters can also be specified on the URL. URL parameters will override parameters in the shortcode. If you override explain, just about ANY value other than 0 will evaluate to true.

