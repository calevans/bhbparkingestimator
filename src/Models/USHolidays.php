<?php

/**
 *
 * Holiday list and rules come from https://www.law.cornell.edu/uscode/text/5/6103
 * Code started life at  https://stackoverflow.com/questions/14907561/how-to-get-date-for-holidays-using-php
 */

class USHolidays
{
  protected $holidayList = [
    "new_year" => [
      'description' => '',
      'format' => 'first day of january %d',
      'weekendShift' => true,
      'federal' => true
    ],
    "mlk_day" => [
      'description' => '',
      'format' => 'third monday january %d',
      'weekendShift' => false,
      'federal' => true
    ],
    "presidents_day" => [
      'description' => '',
      'format' => 'third monday february %d',
      'weekendShift' => false,
      'federal' => true
    ],
    "memorial_day" => [
      'description' => '',
      'format' => 'Last monday of May %d',
      'weekendShift' => true,
      'federal' => true
    ],
    "juneteenth" => [
      'description' => '',
      'format' => 'june 19 %d',
      'weekendShift' => true,
      'federal' => true
    ],
    "independence_day" => [
      'description' => '',
      'format' => 'july 4 %d',
      'weekendShift' => true,
      'federal' => true
    ],
    "labor_day" => [
      'description' => '',
      'format' => 'september %d first monday',
      'weekendShift' => false,
      'federal' => true
    ],
    "columbus_day" => [
      'description' => '',
      'format' => 'october %d second monday',
      'weekendShift' => false,
      'federal' => true
    ],
    "veterans_day" => [
      'description' => '',
      'format' => 'november 11 %d',
      'weekendShift' => true,
      'federal' => true
    ],
    "thanksgiving_day" => [
      'description' => '',
      'format' => 'fourth thursday november %d',
      'weekendShift' => false,
      'federal' => true
    ],
    "christmas_day" => [
      'description' => '',
      'format' => 'december 25 %d',
      'weekendShift' => true,
      'federal' => true
    ]
    ];

  /**
   * Pass in a holiday slug and a year, get back an array of the date of the
   * holiday and the date it is observed.
   */
  public function get(string $slug, int $year): array
  {
    $returnValue = [ 'date' => null, 'observed' => null ];
    if (! isset($this->holidayList[$slug])) {
      return $returnValue;
    }

    $holiday = $this->holidayList[$slug];
    $returnValue['date'] = new DateTimeImmutable(sprintf($holiday['format'], $year));
    if ($holiday['weekendShift']) {
      $returnValue['observed'] = $this->observedDate(new DateTimeImmutable(sprintf($holiday['format'], $year)));
    } else {
      $returnValue['observed'] = new DateTimeImmutable(sprintf($holiday['format'], $year));
    }

    return $returnValue;
  }

  /**
   * Get a list of all holidays
   */
  public function getList(bool $federalOnly = false): array
  {
    $returnValue = [];
    foreach ($this->holidayList as $slug => $thisHoliday) {
      if ($federalOnly && ! $thisHoliday['federal']) {
        continue;
      }
      $returnValue[] = ['slug' => $slug, 'description' => $thisHoliday['description']];
    }
    return $returnValue;
  }

  /**
   * If the observance of the holiday is shifted because of the weekend.
   */
  protected function observedDate(DateTimeImmutable $holiday)
  {
    $day = $holiday->format('w');

    if ($day == 6) {
      $returnValue = $holiday->sub(new DateInterval('P1D'));
    } elseif ($day == 0) {
      $returnValue = $holiday->add(new DateInterval('P1D'));
    } else {
      $returnValue = $holiday;
    }
    return $returnValue;
  }
}
