<?php

/**
 * Parking Estimator for the Blue Heron Bridge
 *
 * The Blue Heron Bridge has unique parking constraints. To get a parking space
 * you need to arrive early. The question si always, how early? This is a simple
 * calculator based on several factors to tell people how early they should
 * arrive to secure parking.
 */


class BHBParkingEstimator
{
  /*
   * Given the date passed in, estimate how many minutes before high tide
   * a diver needs to arrive to get a parking space.
   *
   * Start with 90 minutes
   * ✅ is it a weekday? +0
   * ✅ Is it Sat or Sun +120
   * ✅ is it a holiday +60
   * ✅ Is tide after 10:30 +30
   * ✅ Is tide within an hour of sundown -60
   * ✅ Is it a sunny day? +30
   * ✅ Is the temp 85 or higher +30
   * ✅ Sanity check. Never suggest more than sunrise-1hr
   *
   */
  public function __construct(protected $holidayCalculator = null)
  {
  }
  public function estimate(string $date): array
  {
    $returnValue = [
      'am' => [],
      'pm' => []
      ];
      /*
       * look to see if the tide-table-client is installed, if not, bail because
       * the data we need won't be here.
       */
    if (! \is_plugin_active('tide-tables-client/tide-table-client.php')) {
      return $returnValue;
    }

    $dateToCheck = new \DateTimeImmutable($date);
    $transientKey = 'tide_client_8722588_' . $dateToCheck->format('Ymd');
    $transientData = get_transient($transientKey);

    $todaysData = $transientData['dates'][$dateToCheck->format('Y-m-d')];

    if (! isset($todaysData['tides']['high'])) {
      $returnValue['am']['minutes'] = 0;
      $returnValue['am']['messages'][] = $dateToCheck->format('Y-m-d') . ' is outside of our range of dates.';
      $returnValue['pm']['minutes'] = 0;
      $returnValue['pm']['messages'][] = $dateToCheck->format('Y-m-d') . ' is outside of our range of dates.';
      return $returnValue;
    }


    $sunrise = new DateTimeImmutable($todaysData['ipgeo']['date'] . ' ' . $transientData['dates'][$dateToCheck->format('Y-m-d')]['ipgeo']['sunrise'] . ' AM');
    $sunset  = new DateTimeImmutable($todaysData['ipgeo']['date'] . ' ' . $transientData['dates'][$dateToCheck->format('Y-m-d')]['ipgeo']['sunset'] . ' PM');

    $tideData = $todaysData['tides']['high'];

    if ($tideData['am'] === '--') {
          /*
           * NO AM HIGH TIDE
           */
      $returnValue['am']['minutes'] = 0;
      $returnValue['am']['messages'][] = 'There is no PM High tide';
    } else {
      $amHighTide = new DateTimeImmutable($tideData['am']['date'] . ' ' . $tideData['am']['time'] . 'AM');
      $returnValue['am']['hightide'] = $amHighTide;
          /*
           * Check to make sure high tide is not too early
           */
      if ($amHighTide->sub(new DateInterval('PT1H')) < $sunrise) {
        $returnValue['am']['minutes'] = 0;
        $returnValue['am']['messages'][] = 'High tide is too early for a morning dive.';
      } else {
        $returnValue['am']['minutes'] = 90;
        $returnValue['am']['messages'][] = 'Starting Point            : 90';
        $returnValue['am']['minutes'] += $this->isWeekend($amHighTide);
        $returnValue['am']['messages'][] = 'Weekend Modifier          : ' . $this->isWeekend($amHighTide);
        $returnValue['am']['minutes'] += $this->isHoliday($amHighTide);
        $returnValue['am']['messages'][] = 'Holiday Modifier          : ' . $this->isHoliday($amHighTide);
        $returnValue['am']['minutes'] += $this->isBeachSeason($amHighTide);
        $returnValue['am']['messages'][] = 'Beach Season Modifier     : ' . $this->isBeachSeason($amHighTide);
        $returnValue['am']['minutes'] += $this->tempCheck($amHighTide, $todaysData);
        $returnValue['am']['messages'][] = 'Temperature Modifier      : ' . $this->tempCheck($amHighTide, $todaysData);
        $returnValue['am']['minutes'] += $this->isEarlyMorning($amHighTide);
        $returnValue['am']['messages'][] = 'Early Morning Modifier    : ' .  $this->closeToSundown($amHighTide, $sunset);

              /*
              * Sanity Check If we are recommending that people arrive before sunrise-30M
              * then use sunrise-1h as the arrival time.
              */
        $thisInterval = new DateInterval('PT30M');
        if ($amHighTide->sub(new DateInterval('PT' . $returnValue['am']['minutes'] . 'M')) < $sunrise->sub($thisInterval)) {
          $arrivalTime = $sunrise->sub($thisInterval);
          $interval = $amHighTide->diff($arrivalTime, true);
          $intervalMinutes = ($interval->d * 1440) + ($interval->h * 60) + ($interval->i);
          $returnValue['am']['minutes'] = $intervalMinutes;
          $returnValue['am']['messages'][] = 'Sanity Check Override     : ' . $intervalMinutes;
        }
      }
    }

    if ($tideData['pm'] === '--') {
          /*
           * NO PM HIGH TIDE
           */
      $returnValue['pm']['minutes'] = 0;
      $returnValue['pm']['messages'][] = 'There is no PM High tide';
    } else {
      $pmHighTide = new DateTimeImmutable($tideData['pm']['date'] . ' ' . $tideData['pm']['time']  . 'PM');
      $returnValue['pm']['hightide'] = $pmHighTide;
          /*
           *  Check to make sure high tide is not too late.
           */

      $parkClose = new DateTimeImmutable($tideData['pm']['date'] . '22:00');
      if ($pmHighTide > $parkClose->sub(new DateInterval('PT30M'))) {
        $returnValue['pm']['minutes'] = 0;
        $returnValue['pm']['messages'][] = 'High tide is too late for an evening dive.';
      } else {
        $returnValue['pm']['minutes'] = 90;
        $returnValue['pm']['messages'][] = 'Starting Point            : 90';
        $returnValue['pm']['minutes'] += $this->isWeekend($pmHighTide);
        $returnValue['pm']['messages'][] = 'Weekend Modifier          : ' . $this->isWeekend($pmHighTide);
        $returnValue['pm']['minutes'] += $this->isHoliday($pmHighTide);
        $returnValue['pm']['messages'][] = 'Holiday Modifier          : ' . $this->isHoliday($pmHighTide);
        $returnValue['pm']['minutes'] += $this->isBeachSeason($pmHighTide);
        $returnValue['pm']['messages'][] = 'Beach Season Modifier     : ' . $this->isBeachSeason($pmHighTide);
        $returnValue['pm']['minutes'] += $this->tempCheck($amHighTide, $todaysData);
        $returnValue['pm']['messages'][] = 'Temperature Modifier      : ' . $this->tempCheck($amHighTide, $todaysData);
        $returnValue['pm']['minutes'] += $this->closeToSundown($pmHighTide, $sunset);
        $returnValue['pm']['messages'][] = 'Close To Sundown Modifier : ' .  $this->closeToSundown($pmHighTide, $sunset);

        $thisInterval = new DateInterval('PT60M');
        if ($pmHighTide->sub(new DateInterval('PT' . $returnValue['pm']['minutes'] . 'M')) > $sunset->add($thisInterval)) {
          $arrivalTime = $sunset->add($thisInterval);
          $interval = $pmHighTide->diff($arrivalTime, true);
          $intervalMinutes = ($interval->d * 1440) + ($interval->h * 60) + ($interval->i);
          $returnValue['pm']['minutes'] = $intervalMinutes;
          $returnValue['pm']['messages'][] = 'Sanity Check Override     : ' . $intervalMinutes;
        }
      }
    }

    return $returnValue;
  }

  /*
   * Add 2 hours if it's a weekend
   */
  protected function isWeekend(DateTimeInterface $date): int
  {
    return ($date->format('N') >= 6) ? 120 : 0;
  }

  /*
   * Add 2 hours if it's a weekend
   */
  protected function isEarlyMorning(DateTimeInterface $date): int
  {
    $early = new DateTimeImmutable($date->format('m/d/Y') . ' 07:00:00');
    return ( $date < $early ) ? -60 : 0;
  }

  /**
   * Add 2 hours if it's a holiday.
   */
  protected function isHoliday(DateTimeInterface $date): int
  {
    return $this->holidayCalculator->isHoliday($date) ? 90 : 0;
  }

  /**
   * Subtract 60 minutes if we are within 1 hour of sunset
   */
  protected function closeToSundown(DateTimeInterface $date, DateTimeInterface $sunset): int
  {
    return ( $date->diff($sunset)->h <= 1 ) ? -60 : 0;
  }

  /**
   * Add 60 minutes if we are n Beach Season (May-Sept)
   * Subtract 30 if not
   */
  protected function isBeachSeason(DateTimeInterface $date): int
  {
    $month = (int)$date->format('m');
    return ( $month >= 5 && $month <= 9 ) ? 60 : -30;
  }

  protected function tempCheck(DateTimeInterface $date, array $dateInfo): int
  {
    return ($dateInfo['weather']['temp']['max'] > 85) ? 30 : 0;
  }
}
