<?php

class HolidayCalculator
{
  public function __construct(protected $holidays = null)
  {
  }

  /**
   * Is the date passed in a holiday. This will return true if it is the date
   * of a holiday or the date of the observance of a holiday.
   *
   * If strict is set to true then it will ignore the observance date and only
   * calculate based on the date of the holiday.
   */
  public function isHoliday(DateTimeImmutable $date, bool $strict = false): bool
  {
    foreach ($this->holidays->getList() as $thisHoliday) {
      $holiday = $this->holidays->get($thisHoliday['slug'], $date->format('Y'));

      if ($holiday === []) {
        return false;
      }

      if ($date == $holiday['date'] || (!$strict && $date == $holiday['observed'])) {
        return true;
      }
    }
    return false;
  }
}
