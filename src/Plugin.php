<?php

/**
 * This is the client for the tide-table-api project.
 *
 * This WordPress plugin makes a call to a website running tide-table-api.
 */
class BHBParkingEstimatorPlugin
{
  const OPTIONS_KEY = 'bhb_parking_estimator_options';

  protected string $pluginDir = '';
  protected string $pluginUrl = '';

  public function __construct(protected BHBParkingEstimator $estimator)
  {
    $this->pluginDir = plugin_dir_path(__FILE__);
    $this->pluginUrl = plugin_dir_url(__FILE__);
  }

  /**
   * Setup things
   */
  public function init(): void
  {
    require_once(ABSPATH . 'wp-admin/includes/plugin.php');
    \add_shortcode('bhb-parking-estimator', [$this,'displayShortcode']);
  }

  /**
   * The heart of the client system.  This is called when WordPress
   * encounters a shortcode.
   *
   * [bhb-parking-estimator date=? days = 1 explain=false]
   *
   * date =     The date to show parking for. If days is specific they are
   *            added to this date. If this is not specified, today is used.
   * days =     The number of days from date to check. If date is not specified
   *            then it uses todays date. Setting this to 1 will mean that you
   *            show tomorrow's parking recommendations.
   * explain = Largely for debugging purposes. If set to 1 then it will show the
   *           modifiers used to compute the recommended times.
   *
   * All parameters can also be specified on the URL. URL parameters will
   * override parameters in the shortcode.
   */
  public function displayShortcode($parameters): string
  {
    $parameters = shortcode_atts(
        ['date' => date('Y/m/d'), 'days' => 0, 'explain' => false],
        $parameters
    );


    if (isset($_GET['date'])) {
      $parameters['date'] = (new \DateTimeImmutable($_GET['date']))->format('Y/m/d');
    }
    if (isset($_GET['explain'])) {
      $parameters['explain'] = (bool)$_GET['explain'];
    }

    $options = json_decode(get_option(self::OPTIONS_KEY), true);
    $targetDate = new DateTimeImmutable($parameters['date']);
    if ($parameters['days'] > 0) {
      $targetDate = $targetDate->add(new DateInterval('P' . $parameters['days'] . 'D'));
    }

    $minutesBeforeHighTideArray = $this->estimator->estimate($targetDate->format('Y/m/d'));

    $returnValue = '';

      // AM
    if ($minutesBeforeHighTideArray['am']['minutes'] > 0) {
      $returnValue .= 'For the AM tide, you should arrive at the Blue Heron Bridge around ' . $minutesBeforeHighTideArray['am']['hightide']->sub(new DateInterval('PT' . $minutesBeforeHighTideArray['am']['minutes'] . 'M'))->format('h:i A')   . ' <br>';
    } else {
      $returnValue .= implode('<br>', $minutesBeforeHighTideArray['am']['messages']) . '<br>';
    }

      // PM
    if ($minutesBeforeHighTideArray['pm']['minutes'] > 0) {
      $returnValue .= 'For the PM tide, you should arrive at the Blue Heron Bridge around ' . $minutesBeforeHighTideArray['pm']['hightide']->sub(new DateInterval('PT' . $minutesBeforeHighTideArray['pm']['minutes'] . 'M'))->format('h:i A')   . ' <br>';
    } else {
      $returnValue .= implode('<br>', $minutesBeforeHighTideArray['pm']['messages']) . '<br>';
    }

    if ((bool)$parameters['explain']) {
      if (count($minutesBeforeHighTideArray['am']['messages']) > 0) {
        $returnValue .= '<br>AM Explinations<br><pre>' . implode('<br>', $minutesBeforeHighTideArray['am']['messages']) . '</pre><br>';
      }
      if (count($minutesBeforeHighTideArray['pm']['messages']) > 0) {
        $returnValue .= '<br>PM Explinations<br><pre>' . implode('<br>', $minutesBeforeHighTideArray['pm']['messages']) . '</pre><br>';
      }
    }
    return $returnValue;
  }

  protected function convertToHoursMins($time, $format = '%02d:%02d')
  {
    if ($time < 1) {
      return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
  }
}
